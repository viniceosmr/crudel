# viniexplorador

## Como usar

Para clonar e todar este repositori você precisará de [Git](https://git-scm.com) e [Node.js](https://nodejs.org/en/download/) (que vem com [npm](http://npmjs.com)) instalado no seu computador. Linha de comando:

```bash
# Clone this repository
git clone https://github.com/viniceosm/viniexplorador
# Go into the repository
cd viniexplorador
# Install dependencies
npm install
# Run the app
npm start
```

Note: Se você está usando Linux Bash for Windows, [veja esse link](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) ou use `node` no command prompt.
