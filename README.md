# CrudEl

## Como usar

Para clonar e rodar este repositorio voc� precisar� de [Git](https://git-scm.com) e [Node.js](https://nodejs.org/en/download/) (que vem com [npm](http://npmjs.com)) instalado no seu computador. Linha de comando:

```bash
# Clone this repository
git clone https://viniceosmr@bitbucket.org/viniceosmr/crudel.git
# Go into the repository
cd crudel
# Install dependencies
npm install
# Run the app
npm start
```